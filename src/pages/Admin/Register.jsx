import {
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  Typography,
} from "@mui/material"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Link, useNavigate } from "react-router-dom"
import { toast } from "react-toastify"
import { register, reset } from "../../features/auth/authSlice"

function Register() {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    phone: "",
    password: "",
    password2: "",
  })

  const { name, email, phone, password, password2 } = formData
  const { user, isError, isSuccess, message } = useSelector(
    (state) => state.auth
  )

  useEffect(() => {
    if (isError) {
      toast.error(message)
    }
    if (isSuccess || user) {
      navigate("/")
    }
    dispatch(reset())
  }, [user, isError, isSuccess, message, navigate, dispatch])

  const onChange = (e) => {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }))
  }

  const onSubmit = (e) => {
    e.preventDefault()
    if (password !== password2) {
      toast.error("Konfirmasi password tidak sesuai")
    } else {
      const userData = { name, email, phone, password }
      dispatch(register(userData))
    }
  }

  return (
    <>
      <Typography align='center' variant='h5'>
        Registrasi Admin
      </Typography>
      <Grid>
        <Card
          variant=''
          style={{ maxWidth: 650, padding: "0 5px", margin: "0 auto" }}
        >
          <CardContent>
            <form>
              <Grid container spacing={1}>
                <Grid item xs={12}>
                  <TextField
                    name='name'
                    label='Nama'
                    value={name}
                    onChange={onChange}
                    variant='standard'
                    fullWidth
                    required
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    name='email'
                    label='Email'
                    value={email}
                    onChange={onChange}
                    variant='standard'
                    fullWidth
                    required
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    name='phone'
                    label='No. Telp/HP'
                    value={phone}
                    onChange={onChange}
                    variant='standard'
                    fullWidth
                    required
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    name='password'
                    label='Password'
                    value={password}
                    onChange={onChange}
                    variant='standard'
                    type='password'
                    fullWidth
                    required
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    name='password2'
                    label='Konfirmasi Password'
                    value={password2}
                    onChange={onChange}
                    variant='standard'
                    type='password'
                    fullWidth
                    required
                  />
                </Grid>
                <Grid item xs={12}>
                  <Button
                    onClick={onSubmit}
                    size='large'
                    style={{ margin: "20px auto" }}
                    type='submit'
                    variant='contained'
                    color='primary'
                    fullWidth
                  >
                    Daftar
                  </Button>
                </Grid>
              </Grid>
            </form>
            <Typography align='center' variant='body2'>
              Lakukan{" "}
              <Link
                to='/admins/login'
                style={{ textDecoration: "none", color: "#1976D2" }}
              >
                login
              </Link>{" "}
              bila sudah memiliki akun.
            </Typography>
          </CardContent>
        </Card>
      </Grid>
    </>
  )
}

export default Register
