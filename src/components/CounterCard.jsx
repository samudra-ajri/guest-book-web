import { Card, CardContent, Typography } from "@mui/material"

function GuestCounterCard(props) {
  const { count, label } = props
  return (
    <>
      <Card sx={{ mb: 0.5 }} align='center'>
        <CardContent
          sx={{
            padding: 2,
            "&:last-child": {
              paddingBottom: 2,
            },
          }}
        >
          <Typography variant='h5' pt={2}>
            {count}
          </Typography>
          <Typography variant='body2'>{label}</Typography>
        </CardContent>
      </Card>
    </>
  )
}

export default GuestCounterCard
