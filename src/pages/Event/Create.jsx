import {
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  Typography,
} from "@mui/material"
import { DateTimePicker, LocalizationProvider } from "@mui/x-date-pickers"
import moment from "moment/moment"
import { useEffect, useState } from "react"
import { AdapterMoment } from "@mui/x-date-pickers/AdapterMoment"
import { useNavigate } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { createEvent, reset } from "../../features/event/eventSlice"

function CreateEvent() {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const { user } = useSelector((state) => state.auth)
  const { message } = useSelector((state) => state.event)

  useEffect(() => {
    if (!user) {
      navigate("/admins/login")
    }
    if (message) {
      navigate("/")
    }
    dispatch(reset())
  }, [user, message, navigate, dispatch])

  const [startDate, setStartTime] = useState(moment())
  const handleStartTimeChange = (newValue) => {
    setStartTime(newValue)
  }

  const [endDate, setEndTime] = useState(moment().add(1, "hours"))
  const handleEndTimeChange = (newValue) => {
    setEndTime(newValue)
  }

  const [formData, setFormData] = useState({
    name: "",
    location: "",
  })

  const { name, location } = formData

  const onChange = (e) => {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }))
  }

  const onSubmit = (e) => {
    e.preventDefault()
    const data = { name, location, startDate, endDate }
    dispatch(createEvent(data))
  }

  return (
    <>
      <Typography align='center' variant='h5'>
        Tambah Kegiatan
      </Typography>
      <Grid>
        <Card
          variant=''
          style={{ maxWidth: 650, padding: "0 5px", margin: "0 auto" }}
        >
          <CardContent>
            <form>
              <Grid container spacing={1}>
                <Grid item xs={12}>
                  <TextField
                    name='name'
                    label='Nama Kegiatan'
                    value={name}
                    onChange={onChange}
                    variant='standard'
                    fullWidth
                    required
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    name='location'
                    label='Lokasi'
                    value={location}
                    onChange={onChange}
                    variant='standard'
                    fullWidth
                    required
                  />
                </Grid>
                <Grid item sm={6} xs={12} mt={3}>
                  <LocalizationProvider dateAdapter={AdapterMoment}>
                    <DateTimePicker
                      name='startDate'
                      label='Waktu Mulai'
                      value={startDate}
                      onChange={handleStartTimeChange}
                      renderInput={(params) => <TextField {...params} />}
                      inputFormat='DD/MM/YYYY HH:mm'
                    />
                  </LocalizationProvider>
                </Grid>
                <Grid item sm={6} xs={12} mt={3}>
                  <LocalizationProvider dateAdapter={AdapterMoment}>
                    <DateTimePicker
                      name='endDate'
                      label='Waktu Selesai'
                      value={endDate}
                      onChange={handleEndTimeChange}
                      renderInput={(params) => <TextField {...params} />}
                      inputFormat='DD/MM/YYYY HH:mm'
                    />
                  </LocalizationProvider>
                </Grid>
                <Grid item xs={12}>
                  <Button
                    onClick={onSubmit}
                    size='large'
                    style={{ margin: "20px auto" }}
                    type='submit'
                    variant='contained'
                    color='primary'
                    fullWidth
                  >
                    Tambah
                  </Button>
                </Grid>
              </Grid>
            </form>
          </CardContent>
        </Card>
      </Grid>
    </>
  )
}

export default CreateEvent
