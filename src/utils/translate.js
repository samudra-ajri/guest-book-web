const translate = {}

const daysWeek = {
    Monday: 'Senin',
    Tuesday: 'Selasa',
    Wednesday: 'Rabu',
    Thursday: 'Kamis',
    Friday: 'Jumat',
    Saturday: 'Sabtu',
    Sunday: 'Minggu',
}

translate.days = (word) => {
    return daysWeek[word]
}

export default translate