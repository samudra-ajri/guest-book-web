import { Button, Typography } from "@mui/material"
import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import EventCard from "../../components/EventCard"
import { getEvents, reset } from "../../features/event/eventSlice"

function ListEvent() {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const { user } = useSelector((state) => state.auth)
  const { events } = useSelector((state) => state.event)
  useEffect(() => {
    if (!user) {
      navigate("/admins/login")
    }
    dispatch(getEvents())
    dispatch(reset())
  }, [user, navigate, dispatch])

  const onClick = () => {
    navigate("/events/create")
  }
  return (
    <>
      <Typography align='center' variant='h5'>
        Jadwal Kegiatan
      </Typography>
      <Button
        size='medium'
        style={{ margin: "20px auto" }}
        type='submit'
        variant='contained'
        color='info'
        fullWidth
        onClick={onClick}
      >
        Tambah
      </Button>
      {events.map((event) => (
        <EventCard key={event._id} event={event} />
      ))}
      {events.length === 0 && (
        <Typography
          textAlign='center'
          pt={3}
          color='text.secondary'
          variant='body2'
        >
          Belum ada kegiatan.
        </Typography>
      )}
    </>
  )
}

export default ListEvent
