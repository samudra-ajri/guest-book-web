import {
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  Typography,
} from "@mui/material"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useNavigate, useParams } from "react-router-dom"
import { createGuest, reset } from "../../features/guest/guestSlice"
import { toast } from "react-toastify"
import { getEvent } from "../../features/event/eventSlice"
import moment from "moment"

function Presence() {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { eventId } = useParams()
  const [formData, setFormData] = useState({
    name: "",
    phone: "",
    address: "",
  })
  const { name, phone, address } = formData
  const { isSuccess, isError, message } = useSelector((state) => state.guest)
  const {
    event,
    isError: isErrorEvent,
    isSuccess: isSuccessEvent,
    message: messageEvent,
  } = useSelector((state) => state.event)

  useEffect(() => {
    if (isError) {
      toast.error(message)
    }

    if (isErrorEvent) {
      navigate('/guests/event')
    }

    if (isSuccess) {
      toast.success('Yeay! Kehadiran berhasil ditambahkan')
      setFormData({
        name: "",
        phone: "",
        address: "",
      })
    }

    if (eventId) dispatch(getEvent(eventId))
    dispatch(reset())
  }, [isSuccess, isError, isErrorEvent, message, messageEvent, eventId, navigate, dispatch])

  const onChange = (e) => {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }))
  }

  const onSubmit = (e) => {
    e.preventDefault()
    const data = { event: eventId, name, phone, address }
    dispatch(createGuest(data))
  }

  return (
    <>
      {isSuccessEvent ? (
        <>
          <Typography align='center' variant='h5'>
            {event.name}
          </Typography>
          <Typography align='center' variant='h6'>
            {event.location}
          </Typography>
          <Typography align='center' variant='subtitle2'>
            {moment(event.startDate).locale("id").format("LLLL")}
          </Typography>
        </>
      ) : (
        <Typography align='center' variant='h6'>
          Kegiatan tidak ditemukan.
        </Typography>
      )}

      <Grid>
        <Card
          variant=''
          style={{ maxWidth: 650, padding: "0 5px", margin: "0 auto" }}
        >
          <CardContent>
            <form>
              <Grid container spacing={1}>
                <Grid item xs={12}>
                  <TextField
                    name='name'
                    label='Nama Lengkap'
                    value={name}
                    onChange={onChange}
                    variant='standard'
                    fullWidth
                    required
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    name='phone'
                    label='No. HP/Telp'
                    value={phone}
                    onChange={onChange}
                    variant='standard'
                    fullWidth
                    required
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    name='address'
                    label='Alamat/Instansi'
                    value={address}
                    onChange={onChange}
                    variant='standard'
                    fullWidth
                    required
                  />
                </Grid>
                <Grid item xs={12}>
                  <Button
                    onClick={onSubmit}
                    size='large'
                    style={{ margin: "20px auto" }}
                    type='submit'
                    variant='contained'
                    color='primary'
                    fullWidth
                  >
                    Hadir
                  </Button>
                </Grid>
              </Grid>
            </form>
          </CardContent>
        </Card>
      </Grid>
    </>
  )
}

export default Presence
