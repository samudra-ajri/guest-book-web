import API from '../api'
const API_URL = 'api/admins/'
const adminService = {}

// List admins
adminService.list = async (token) => {
    const config = {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    }
    const response = await API.get(API_URL, config)
    return response.data?.data
}

// Delete admin
adminService.delete = async (id, token) => {
    const config = {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    }
    const response = await API.delete(API_URL + id, config)
    return response.data
}

// My profile admin
adminService.me = async (token) => {
    const config = {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    }
    const response = await API.get(API_URL + 'me', config)
    return response.data?.data
}

// Update profile admin
adminService.update = async (data, token) => {
    const config = {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    }
    const response = await API.put(API_URL + 'me', data, config)
    return response.data
}

export default adminService