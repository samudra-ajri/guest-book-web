import { Typography } from "@mui/material"
import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import PeopleCard from "../../components/PeopleCard"
import { getAdmins, reset } from "../../features/admin/adminSlice"

function ListAdmin() {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const { user } = useSelector((state) => state.auth)
  const { admins } = useSelector((state) => state.admin)

  useEffect(() => {
    if (!user) {
      navigate("/admins/login")
    }
    dispatch(getAdmins())
    dispatch(reset())
  }, [user, navigate, dispatch])

  return (
    <>
      <Typography align='center' variant='h5'>
        Daftar Admin
      </Typography>
      {admins.map((admin) => (
        <PeopleCard key={admin._id} user={admin} />
      ))}
    </>
  )
}

export default ListAdmin
