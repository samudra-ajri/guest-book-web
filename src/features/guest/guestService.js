import API from '../api'
const API_URL = 'api/guests/'
const guestService = {}

// Create guest presence
guestService.create = async (data) => {
    const response = await API.post(API_URL, data)
    return response.data
}

// Get guest list by event
guestService.guestsByEvent = async (eventId, token) => {
    const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    const response = await API.get(API_URL + `event/${eventId}`, config)
    return response.data
}

export default guestService