import { Card, CardContent, Typography } from "@mui/material"
import moment from "moment"

function GuestCard(props) {
  const { guest } = props

  const presenceTime = (time) => {
    const startTime = moment(time).format("HH:mm")
    const startDayName = moment(time).format("dddd")
    return `${startDayName}, ${startTime}`
  }

  return (
    <>
      <Card key={guest._id} sx={{ mb: 0.5 }} align='left'>
        <CardContent sx={{ padding: 2, "&:last-child": { paddingBottom: 2 } }}>
          <Typography fontSize={10} component='p' color='text.secondary'>
            {presenceTime(guest.createdAt)}
          </Typography>
          <Typography variant='body1'>{guest.name}</Typography>
          <Typography fontSize={10} component='p' color='text.secondary'>
            {guest.address}
          </Typography>
          <Typography fontSize={10} component='p' color='text.secondary'>
            {guest.phone}
          </Typography>
        </CardContent>
      </Card>
    </>
  )
}

export default GuestCard
