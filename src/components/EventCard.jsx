import {
  Box,
  Button,
  Card,
  CardActionArea,
  CardContent,
  CircularProgress,
  Grid,
  IconButton,
  Link,
  Stack,
  Typography,
} from "@mui/material"
import DeleteIcon from "@mui/icons-material/DeleteOutlineRounded"
import moment from "moment"
import { useState } from "react"
import PopDialog from "./PopDialog"
import { useDispatch } from "react-redux"
import { deleteEvent, getEvent } from "../features/event/eventSlice"
import { Link as LinkRoute, useNavigate } from "react-router-dom"

function EventCard(props) {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { event } = props
  const [openPopup, setOpenPopup] = useState(false)
  const isLoading = false

  const onClick = () => {
    setOpenPopup(true)
  }

  const onClickRemove = () => {
    dispatch(deleteEvent(event._id))
    navigate('/')
  }

  const eventTime = () => {
    const eventTime = {}
    const startDate = moment(event.startDate).format("DD/MM/YY")
    const startTime = moment(event.startDate).format("HH.mm")
    const endDate = moment(event.endDate).format("DD/MM/YY")
    const endTime = moment(event.endDate).format("HH.mm")
    const startDayName = moment(event.startDate).format("dddd")
    if (startDate === endDate) {
      eventTime.keys = (
        <>
          <Typography variant='body2'>Hari</Typography>
          <Typography variant='body2'>Tanggal</Typography>
          <Typography variant='body2'>Jam</Typography>
        </>
      )
      eventTime.values = (
        <>
          <Typography variant='body2'>: {startDayName}</Typography>
          <Typography variant='body2'>: {startDate}</Typography>
          <Typography variant='body2'>
            : {startTime} - {endTime}
          </Typography>
        </>
      )
    } else {
      eventTime.keys = (
        <>
          <Typography variant='body2'>Hari</Typography>
          <Typography variant='body2'>Mulai</Typography>
          <Typography variant='body2'>Selesai</Typography>
        </>
      )
      eventTime.values = (
        <>
          <Typography variant='body2'>: {startDayName}</Typography>
          <Typography variant='body2'>
            : {startDate} pkl. {startTime}
          </Typography>
          <Typography variant='body2'>
            : {endDate} pkl. {endTime}
          </Typography>
        </>
      )
    }
    return eventTime
  }

  return (
    <>
      <Card sx={{ mb: 0.5, cursor: "pointer" }}>
        <CardContent
          sx={{
            padding: 2,
            "&:last-child": {
              paddingBottom: 2,
            },
          }}
        >
          <Grid container>
            <Grid item xs={10} md={11}>
              <LinkRoute
                to='/events/detail'
                component={CardActionArea}
                style={{ textDecoration: "none", color: "black" }}
                onClick={() => dispatch(getEvent(event._id))}
              >
                <Grid container>
                  <Grid item>
                    <Typography variant='h5'>{event.name}</Typography>
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid item xs={2}>
                    {eventTime().keys}
                    <Typography variant='body2'>Lokasi</Typography>
                  </Grid>
                  <Grid item xs={8}>
                    {eventTime().values}
                    <Typography variant='body2'>: {event.location}</Typography>
                  </Grid>
                </Grid>
              </LinkRoute>
              <Grid container>
                <Grid item>
                  <Link href={`/guests/event/${event._id}`} underline='none'>
                    <Typography variant='body2'>Link Daftar Hadir</Typography>
                  </Link>
                </Grid>
              </Grid>
            </Grid>
            <Grid item>
              <IconButton align='right' onClick={onClick}>
                <DeleteIcon fontSize='medium' color={"error"} />
              </IconButton>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      <PopDialog title={`Hapus ${event.name}?`} openPopup={openPopup}>
        <Box sx={{ display: "flex", justifyContent: "center", height: 45 }}>
          {isLoading ? (
            <Grid align='center' sx={{ pt: 1.5 }}>
              <CircularProgress size={20} />
            </Grid>
          ) : (
            <Stack spacing={1} direction='row'>
              <Button variant='outlined' color='error' onClick={onClickRemove}>
                Hapus
              </Button>
              <Button variant='contained' onClick={() => setOpenPopup(false)}>
                Batal
              </Button>
            </Stack>
          )}
        </Box>
      </PopDialog>
    </>
  )
}

export default EventCard
