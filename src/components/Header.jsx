import { AppBar, Button, IconButton, Toolbar, Typography } from "@mui/material"
import { useDispatch, useSelector } from "react-redux"
import LogoutIcon from "@mui/icons-material/LogoutRounded"
import { Link, useNavigate } from "react-router-dom"
import { logout, reset } from "../features/auth/authSlice"

function Header() {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { user } = useSelector((state) => state.auth)

  const onLogout = () => {
    dispatch(logout())
    dispatch(reset())
    navigate("/")
  }

  return (
    <div>
      <AppBar color='default'>
        <Toolbar>
          <Typography variant='h6' sx={{ flexGrow: 1 }}>
            <b>BukuTamu</b>
          </Typography>
          {user && (
            <>
              {user.role === "999" && (
                <Button color='inherit'>
                  <Link
                    to='/admins/list'
                    style={{ textDecoration: "none", color: "black" }}
                  >
                    Admin
                  </Link>
                </Button>
              )}
              {user.role === "998" && (
                <Button color='inherit'>
                  <Link
                    to='/admins/profile'
                    style={{ textDecoration: "none", color: "black" }}
                  >
                    Profile
                  </Link>
                </Button>
              )}
              <Button color='inherit'>
                <Link to='/' style={{ textDecoration: "none", color: "black" }}>
                  Kegiatan
                </Link>
              </Button>
              <IconButton onClick={onLogout}>
                <LogoutIcon />
              </IconButton>
            </>
          )}
        </Toolbar>
      </AppBar>
    </div>
  )
}

export default Header
