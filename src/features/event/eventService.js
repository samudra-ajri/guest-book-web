import API from '../api'
const API_URL = 'api/events/'
const eventService = {}

// Get event by id
eventService.detail = async (id) => {
  const response = await API.get(API_URL + id)
  return response.data?.data
}

// Create event
eventService.create = async (data, token) => {
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }
  const response = await API.post(API_URL, data, config)
  return response.data
}

// Delete event
eventService.delete = async (id, token) => {
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }
  const response = await API.delete(API_URL + id, config)
  return response.data
}

// List events
eventService.list = async () => {
  const response = await API.get(API_URL)
  return response.data?.data
}
export default eventService