import API from '../api'
const API_URL = 'api/admins/'
const authService = {}

// Register user
authService.register = async (userData) => {
    const response = await API.post(API_URL, userData)
    if (response.data) {
        localStorage.setItem('user', JSON.stringify(response.data.data))
    }
    return response.data?.data
}

// Login user
authService.login = async (userData) => {
    const response = await API.post(API_URL + 'auth', userData)
    if (response.data) {
        localStorage.setItem('user', JSON.stringify(response.data.data))
    }
    return response.data?.data
}

// Logout user
authService.logout = () => {
    localStorage.removeItem('user')
}


export default authService