import { Typography } from "@mui/material"
import EventCard from "../../components/EventCard"
import GuestCard from "../../components/GuestCard"
import CounterCard from "../../components/CounterCard"
import { useDispatch, useSelector } from "react-redux"
import { useEffect } from "react"
import { useNavigate } from "react-router-dom"
import { guestsByEvent } from "../../features/guest/guestSlice"

function DetailEvent() {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const { event, isLoading } = useSelector((state) => state.event)
  const { guests, total } = useSelector((state) => state.guest)

  useEffect(() => {
    if (!event && !isLoading) {
      navigate("/")
    } 
    if (event) {
      dispatch(guestsByEvent(event._id))
    }
  }, [event, isLoading, navigate, dispatch])

  if (!event) {
    return
  }

  return (
    <>
      <Typography align='center' variant='h5'>
        Daftar Hadir
      </Typography>
      <EventCard event={event} />
      <CounterCard count={total} label='hadir' />
      {guests.map((guest) => (
        <GuestCard key={guest._id} guest={guest} />
      ))}
    </>
  )
}

export default DetailEvent
