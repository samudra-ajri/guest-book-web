import { Typography } from "@mui/material"
import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import { toast } from "react-toastify"
import AdminDetailCard from "../../components/AdminDetailCard"
import { me } from "../../features/admin/adminSlice"

function AdminProfile() {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const { admin, isError, message } = useSelector(
    (state) => state.admin
  )

  useEffect(() => {
    if (isError) {
      toast.error(message)
    }
    dispatch(me())
  }, [isError, message, navigate, dispatch])

  return (
    <>
      <Typography align='center' variant='h5'>
        Profile Admin
      </Typography>
      <AdminDetailCard user={admin} />
    </>
  )
}

export default AdminProfile
