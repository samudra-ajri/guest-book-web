import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import guestService from './guestService'

const initialState = {
    guests: [],
    total: 0,
    isError: false,
    isSuccess: false,
    isLoading: false,
    message: ''
}

// Create guest presence
export const createGuest = createAsyncThunk(
    'guests/create',
    async (guest, thunkAPI) => {
        try {
            return await guestService.create(guest)
        } catch (error) {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString()
            return thunkAPI.rejectWithValue(message)
        }
    }
)

// List guests by event
export const guestsByEvent = createAsyncThunk(
    'guests/event',
    async (eventId, thunkAPI) => {
        try {
            const token = thunkAPI.getState().auth.user.token
            return await guestService.guestsByEvent(eventId, token)
        } catch (error) {
            const message =
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString()
            return thunkAPI.rejectWithValue(message)
        }
    }
)

export const guestSlice = createSlice({
    name: 'guest',
    initialState,
    reducers: {
        reset: (state) => {
            state.isLoading = false
            state.isSuccess = false
            state.isError = false
            state.message = ''
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(createGuest.pending, (state) => {
                state.isLoading = true
            })
            .addCase(createGuest.fulfilled, (state) => {
                state.isLoading = false
                state.isSuccess = true
            })
            .addCase(createGuest.rejected, (state, action) => {
                state.isLoading = false
                state.isError = true
                state.message = action.payload
            })
            .addCase(guestsByEvent.pending, (state) => {
                state.isLoading = true
            })
            .addCase(guestsByEvent.fulfilled, (state, action) => {
                state.isLoading = false
                state.isSuccess = true
                state.guests = action.payload.data
                state.total = action.payload.total
            })
            .addCase(guestsByEvent.rejected, (state, action) => {
                state.isLoading = false
                state.isError = true
                state.message = action.payload
            })
    },
})

export const { reset } = guestSlice.actions
export default guestSlice.reducer