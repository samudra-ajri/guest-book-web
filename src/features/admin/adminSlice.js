import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import adminService from './adminService'

const initialState = {
    admin: null,
    admins: [],
    isError: false,
    isSuccess: false,
    isLoading: false,
    message: ''
}

// List admins
export const getAdmins = createAsyncThunk(
    'admins/list',
    async (_, thunkAPI) => {
        try {
            const token = thunkAPI.getState().auth.user.token
            return await adminService.list(token)
        } catch (error) {
            const message =
                (
                    error.response &&
                    error.response.data &&
                    error.response.data.message
                ) ||
                error.message ||
                error.toString()
            return thunkAPI.rejectWithValue(message)
        }
    }
)

// Delete admin
export const deleteAdmin = createAsyncThunk(
    'admins/delete',
    async (id, thunkAPI) => {
        try {
            const token = thunkAPI.getState().auth.user.token
            return await adminService.delete(id, token)
        } catch (error) {
            const message =
                (
                    error.response &&
                    error.response.data &&
                    error.response.data.message
                ) ||
                error.message ||
                error.toString()
            return thunkAPI.rejectWithValue(message)
        }
    }
)

// Admins my profile
export const me = createAsyncThunk(
    'admins/me',
    async (_, thunkAPI) => {
        try {
            const token = thunkAPI.getState().auth.user.token
            return await adminService.me(token)
        } catch (error) {
            const message =
                (
                    error.response &&
                    error.response.data &&
                    error.response.data.message
                ) ||
                error.message ||
                error.toString()
            return thunkAPI.rejectWithValue(message)
        }
    }
)

// Update my profile
export const updateProfile = createAsyncThunk(
    'admins/update',
    async (data, thunkAPI) => {
        try {
            const token = thunkAPI.getState().auth.user.token
            return await adminService.update(data, token)
        } catch (error) {
            const message =
                (
                    error.response &&
                    error.response.data &&
                    error.response.data.message
                ) ||
                error.message ||
                error.toString()
            return thunkAPI.rejectWithValue(message)
        }
    }
)

export const adminSlice = createSlice({
    name: 'admin',
    initialState,
    reducers: {
        reset: (state) => {
            state.isLoading = false
            state.isSuccess = false
            state.isError = false
            state.message = ''
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(getAdmins.pending, (state) => {
                state.isLoading = true
            })
            .addCase(getAdmins.fulfilled, (state, action) => {
                state.isLoading = false
                state.isSuccess = true
                state.admins = action.payload
            })
            .addCase(getAdmins.rejected, (state, action) => {
                state.isLoading = false
                state.isError = true
                state.message = action.payload
            })
            .addCase(deleteAdmin.pending, (state) => {
                state.isLoading = true
            })
            .addCase(deleteAdmin.fulfilled, (state, action) => {
                state.isLoading = false
                state.isSuccess = true
                state.admins = state.admins.filter(
                    (admin) => admin._id !== action.payload.message
                )
            })
            .addCase(deleteAdmin.rejected, (state, action) => {
                state.isLoading = false
                state.isError = true
                state.message = action.payload
            })
            .addCase(me.pending, (state) => {
                state.isLoading = true
            })
            .addCase(me.fulfilled, (state, action) => {
                state.isLoading = false
                state.isSuccess = true
                state.admin = action.payload
            })
            .addCase(me.rejected, (state, action) => {
                state.isLoading = false
                state.isError = true
                state.message = action.payload
            })
            .addCase(updateProfile.pending, (state) => {
                state.isLoading = true
            })
            .addCase(updateProfile.fulfilled, (state, action) => {
                state.isLoading = false
                state.isSuccess = true
            })
            .addCase(updateProfile.rejected, (state, action) => {
                state.isLoading = false
                state.isError = true
                state.message = action.payload
            })
    },
})

export const { reset } = adminSlice.actions
export default adminSlice.reducer