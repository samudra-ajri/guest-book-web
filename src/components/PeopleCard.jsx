import {
  Box,
  Button,
  Card,
  CardActionArea,
  CardContent,
  CircularProgress,
  Grid,
  IconButton,
  Stack,
  Tooltip,
  Typography,
} from "@mui/material"
import DeleteIcon from "@mui/icons-material/DeleteOutlineRounded"
import { Link, useNavigate } from "react-router-dom"
import PopDialog from "./PopDialog"
import { useState } from "react"
import { useDispatch } from "react-redux"
import { deleteAdmin } from "../features/admin/adminSlice"

function PeopleCard(props) {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { user } = props
  const [openPopup, setOpenPopup] = useState(false)
  const isLoading = false

  const onClickRemove = () => {
    dispatch(deleteAdmin(user._id))
    navigate("/admins/list")
  }

  const onClick = () => {
    setOpenPopup(true)
  }

  return (
    <>
      <Card sx={{ mb: 0.5, cursor: "pointer" }}>
        <CardContent
          sx={{
            padding: 2,
            "&:last-child": {
              paddingBottom: 2,
            },
          }}
        >
          <Grid container>
            <Grid item xs={10} md={11}>
              <Link
                to={`#`}
                component={CardActionArea}
                style={{ textDecoration: "none", color: "black" }}
              >
                <Grid container>
                  <Grid item>
                    <Typography variant='body1'>{user.name}</Typography>
                  </Grid>
                </Grid>
                <Typography fontSize={10} component='p' color='text.secondary'>
                  {user.email}
                </Typography>
                <Typography fontSize={10} component='p' color='text.secondary'>
                  {user.phone}
                </Typography>
              </Link>
            </Grid>
            <Grid item>
              <Tooltip title='hapus admin'>
                <IconButton align='right' onClick={onClick}>
                  <DeleteIcon fontSize='medium' color='error' />
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      <PopDialog title={`Hapus ${user.name}?`} openPopup={openPopup}>
        <Box sx={{ display: "flex", justifyContent: "center", height: 45 }}>
          {isLoading ? (
            <Grid align='center' sx={{ pt: 1.5 }}>
              <CircularProgress size={20} />
            </Grid>
          ) : (
            <Stack spacing={1} direction='row'>
              <Button variant='outlined' color='error' onClick={onClickRemove}>
                Hapus
              </Button>
              <Button variant='contained' onClick={() => setOpenPopup(false)}>
                Batal
              </Button>
            </Stack>
          )}
        </Box>
      </PopDialog>
    </>
  )
}

export default PeopleCard
