import { configureStore } from '@reduxjs/toolkit'
import authReducer from '../features/auth/authSlice'
import guestReducer from '../features/guest/guestSlice'
import eventReducer from '../features/event/eventSlice'
import adminReducer from '../features/admin/adminSlice'

export const store = configureStore({
  reducer: {
    auth: authReducer,
    guest: guestReducer,
    event: eventReducer,
    admin: adminReducer,
  },
})
