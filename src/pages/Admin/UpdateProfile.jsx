import {
  Button,
  Card,
  CardContent,
  Grid,
  TextField,
  Typography,
} from "@mui/material"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import { toast } from "react-toastify"
import { me, updateProfile } from "../../features/admin/adminSlice"

function AdminProfileUpdate() {
  const navigate = useNavigate()
  const dispatch = useDispatch()

  const { admin, isError, isSuccess, message } = useSelector(
    (state) => state.admin
  )
  const [formData, setFormData] = useState({
    name: admin?.name,
    email: admin?.email,
    phone: admin?.phone,
  })
  const { name, email, phone } = formData

  useEffect(() => {
    if (isError) {
      toast.error(message)
    }
    if (!admin) dispatch(me())
  }, [admin, isError, isSuccess, message, navigate, dispatch])

  const onChange = (e) => {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }))
  }

  const onSubmit = (e) => {
    e.preventDefault()
    const userData = { name, email, phone }
    dispatch(updateProfile(userData))
    navigate('/admins/profile')
  }

  return (
    <>
      <Typography align='center' variant='h5'>
        Update Profile
      </Typography>
      <Grid>
        <Card
          variant=''
          style={{ maxWidth: 650, padding: "0 5px", margin: "0 auto" }}
        >
          <CardContent>
            <form>
              <Grid container spacing={1}>
                <Grid item xs={12}>
                  <TextField
                    name='name'
                    label='Nama'
                    value={name}
                    onChange={onChange}
                    variant='standard'
                    fullWidth
                    required
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    name='email'
                    label='Email'
                    value={email}
                    onChange={onChange}
                    variant='standard'
                    fullWidth
                    required
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    name='phone'
                    label='No. Telp/HP'
                    value={phone}
                    onChange={onChange}
                    variant='standard'
                    fullWidth
                    required
                  />
                </Grid>
                <Grid item xs={12}>
                  <Button
                    onClick={onSubmit}
                    size='large'
                    style={{ margin: "20px auto" }}
                    type='submit'
                    variant='contained'
                    color='primary'
                    fullWidth
                  >
                    Ubah
                  </Button>
                </Grid>
              </Grid>
            </form>
          </CardContent>
        </Card>
      </Grid>
    </>
  )
}

export default AdminProfileUpdate
