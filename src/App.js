import { Container } from '@mui/material'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import Presence from './pages/Guest/Presence'
import Login from './pages/Admin/Login'
import Register from './pages/Admin/Register'
import Header from './components/Header'
import CreateEvent from './pages/Event/Create'
import ListEvent from './pages/Event/List'
import DetailEvent from './pages/Event/Detail'
import ListAdmin from './pages/Admin/List'
import AdminProfile from './pages/Admin/Profile'
import 'moment/locale/id'
import AdminProfileUpdate from './pages/Admin/UpdateProfile'

function App() {
  return (
    <>
      <Router>
        <Header />
        <Container style={{ 'marginTop': 100 }}>
          <Routes>
            <Route path='guests/event/' element={<Presence />} />
            <Route path='guests/event/:eventId' element={<Presence />} />
            <Route path='/events/create' element={<CreateEvent />} />
            <Route path='/events/detail' element={<DetailEvent />} />
            <Route path='/' element={<ListEvent />} />
            <Route path='/admins/list' element={<ListAdmin />} />
            <Route path='/admins/login' element={<Login />} />
            <Route path='/admins/register' element={<Register />} />
            <Route path='/admins/profile' element={<AdminProfile />} />
            <Route path='/admins/profile/update' element={<AdminProfileUpdate />} />
          </Routes>
        </Container>
      </Router>
      <ToastContainer />
    </>
  )
}

export default App;
