import { Card, CardContent, Grid, IconButton, Typography } from "@mui/material"
import EditIcon from "@mui/icons-material/DriveFileRenameOutlineOutlined"
import { useNavigate } from "react-router-dom"

function AdminDetailCard(props) {
  const navigate = useNavigate()
  const { user } = props
  const onClick = () => {
    navigate("/admins/profile/update")
  }
  const isMyProfile = true

  return (
    <>
      <Card sx={{ marginBottom: 2, justifyItems: "center" }}>
        <CardContent>
          <Grid container>
            <Grid item xs={10} md={11}>
              <Typography variant='h5' component='div'>
                {user?.name}
              </Typography>
              <Typography color='text.secondary'>
                {user?.phone && user.phone}
              </Typography>
              <Typography color='text.secondary'>
                {user?.email && user.email}
              </Typography>
            </Grid>
            {isMyProfile && (
              <Grid item>
                <IconButton align='right' onClick={onClick}>
                  <EditIcon fontSize='medium' />
                </IconButton>
              </Grid>
            )}
          </Grid>
        </CardContent>
      </Card>
    </>
  )
}

export default AdminDetailCard
